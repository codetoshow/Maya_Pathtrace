/*------------------------------------------------------------------------------
 This free software incorporates by reference the text of the WTFPL, Version 2
 
 File:  PathTrace.cpp

 Description: Please see the file "PathTrace.h"
        ( Based on the Autodesk Maya Plug-in, footPrintNode )
 
        Created by Ken Mohamed on 6/16/13.
 -----------------------------------------------------------------------------*/
#include <maya/MFloatPoint.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnPointArrayData.h>
#include <maya/MDGModifier.h>
#include <maya/MString.h>
#include <maya/MGlobal.h>
#include <maya/MAnimControl.h>
#include <maya/MTime.h>

#include "PathTrace.h"

// Constants for descriptive array indices
static const int X = 0;
static const int Y = 1;
static const int Z = 2;

// Static member data... must be defined at file-scope.
//      Node Attributes.  
MObject     PathTrace::pointAttr;
MObject     PathTrace::passThroughAttr;
int         PathTrace::pointAccumulatorSize;

//      Standard Maya Plug-in Node initializations
MString     PathTrace::drawDbClassification("drawdb/geometry/PathTrace");
MString     PathTrace::drawRegistrantId    ("PathTrace");

static MStatus    status;

PathTrace:: PathTrace() {
    // Node Instance Internal Data
    // pointAccumulator: accumulates point data that appears at the pointAttr attribute 
    // each time the compute method executes.  The draw method ( which executes 
    // once the compute method completes) draws lines between the points that have 
    // accumulated in pointAccumulator.
    //
    MFnPointArrayData   pointArrayFn;
    pointArrayFn.create( pointAccumulator, &status ); 
    McheckErr_noReturn( status, "Error: could not create pointAccumulator");

    // Limit the size of the pointAccumulator array 
    // to the number of playback frames (from the Maya Range Slider)
    //
    MTime playback_start = MAnimControl::minTime();
    MTime playback_end   = MAnimControl::maxTime();
    int start_frame      = (int) playback_start.as( MTime::uiUnit() );
    int end_frame        = (int) playback_end.as(   MTime::uiUnit() );
    pointAccumulatorSize = end_frame - start_frame;
}

void PathTrace::postConstructor() 
{
    // setting the shape node name explicitely here, lets Maya name the
    // transform node similarly.... i.e. rather than "transform1"
    // [ Tip from Dean Edmonds  ]
    //
    MFnDependencyNode nodeFn( thisMObject() );
    nodeFn.setName( "tracerShape#", &status );
    McheckErr_noReturn( status, "Warning: could not set name of tracer shape node");
}


MStatus PathTrace::initialize() 
{
   
    MFnNumericAttribute numAttrFn; 
    
    // ADD NODE ATTRIBUTES
    //      with createPoint, the data type is predefined 
    //
    pointAttr       = numAttrFn.createPoint( "point", "p", &status);
    McheckErr( status, "Error: could not createPoint pointAttr");
    addAttribute(pointAttr);
    
    passThroughAttr = numAttrFn.createPoint( "passThrough", "pt", &status );
    McheckErr( status, "Error: could not createPoint passThroughAttr");
    numAttrFn.setWritable( FALSE );
    addAttribute( passThroughAttr );
    
    // SET ATTRIBUTE AFFECT-RELATIONSHIPS
    // attributeAffects marks output "dirty" when input changes
    // but compute doesn't fire unless output is requested (... via connection
    // to a downstream DG node )
    //
    attributeAffects( pointAttr, passThroughAttr );

	return MS::kSuccess;
}

MStatus PathTrace::compute( const MPlug& plug, MDataBlock& dataBlock ) 
{
    //  Only do stuff until pointAccumulator fills up.  
    // ( compute() will still fire, however. )
    //
    if ( pointAccumulator.length() < ( pointAccumulatorSize ) )
    {
        // PULL DATA from MDataBlock
        MFloatPoint point = dataBlock.inputValue(pointAttr).asFloatVector();

        // PUSH DATA to MDataBlock
        //      "set" method writes an MFloatPoint out on a node attr, 
        //      and it expects an MFloatVector
        //
        dataBlock.outputValue(passThroughAttr).set((MFloatVector)point);
        
        // Accumulate points
        pointAccumulator.append(point);
    }
    return MS::kSuccess;
}

void PathTrace::draw( M3dView & view, 
                      const MDagPath &,
                      M3dView::DisplayStyle  style,
                      M3dView::DisplayStatus status ) 
{
    static int pointArrayCount = 0;
    // Get the current number of points
    pointArrayCount = pointAccumulator.length();
    

    // Draw line segments (GL_LINES) between points at array indicies [i], [i+1]
    // (but only if there are at least two points)
    //
    if ( pointArrayCount >= 2 ) 
    {
        view.beginGL();
	    glBegin( GL_LINES );

        for ( int i = 0; i < pointArrayCount - 1; i+=1 ) 
        {
            glVertex3f( pointAccumulator[i][X],
                        pointAccumulator[i][Y],
                        pointAccumulator[i][Z]  );
            glVertex3f( pointAccumulator[i+1][X],
                        pointAccumulator[i+1][Y],
                        pointAccumulator[i+1][Z]  );
        }

	    glEnd();
	    view.endGL();
    } 
}

void* PathTrace::creator() 
{
	return new PathTrace();
}

PathTrace::~PathTrace() {}
// ----------------------------------------------------------------------------