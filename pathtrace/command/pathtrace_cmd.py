"""
 This free software incorporates by reference the text of the WTFPL, Version 2

 File:          pathtrace_cmd.py


 Description:   This file defines the "pathtrace" command which is intended to be
                used together with the custom "tracer" node, to create an object
                motion path in the Maya standard viewport.

                This command creates a tracer node as well as two standard locators.
                It makes two connections:
                    * from the "translation" attribute of the 1st locator
                      to the   "pointAttr"   attribute of the tracer node and
                    * from the "passThroughAttr" attribute of the tracer node
                      to the 2nd locator.
"""
# STANDARD IMPORTS
import sys

# THIRD PARTY IMPORTS
import maya.OpenMaya as OpenMaya
import maya.OpenMayaMPx as OpenMayaMPx
import maya.cmds

NODE_TYPE = "tracer"
PLUGIN_CMD_NAME = "pathtrace"


class ScriptedCommand(OpenMayaMPx.MPxCommand):
    """ScriptedCommand

    """
    def __init__(self):
        OpenMayaMPx.MPxCommand.__init__(self)

    @staticmethod
    def do_it(_):
        """
        do_it is called when the pathtrace command is run in the Maya shell.
        :param _:
        """
        # Get the selection
        try:
            target = maya.cmds.ls(selection=True)[0]
        except IndexError:
            print sys.exc_info()[0]
            maya.cmds.confirmDialog(message="Please SELECT a joint and try again.",
                                    button="OK",
                                    icon="warning")
            return

        # ----------------------------------------------------
        # CREATE 3 LOCATORS - one custom and two standard
        #
        #  Locator 1. 'tracer' is a custom locator that
        #  - builds an internal array of points collected from an input attribute
        #  - draws OpenGL line segments between the points as its 'locator shape'
        tracer_shape = maya.cmds.createNode(NODE_TYPE)
        tracer_txfrm = maya.cmds.listRelatives(tracer_shape, parent=True)[0]
        #
        # locator 2. 'traceTarget_locator' is standard locator that
        #  - gets parent-constrained to the target
        #  - pushes position data into 'tracer'
        push_locator = maya.cmds.spaceLocator(name='traceTarget_locator_#')[0]

        #
        # Locator 3. 'force_tracer_compute_#' is another standard locator that
        #  - pulls location data from 'tracer' to force its compute() to run.
        pull_locator = maya.cmds.spaceLocator(name='force_tracer_compute_#')[0]

        # ----------------------------------------------------
        #  CREATE CONNECTIONS BETWEEN THE LOCATORS
        #
        #  Attribute connections
        maya.cmds.connectAttr(push_locator + ".translate", tracer_shape + ".point")
        maya.cmds.connectAttr(tracer_shape + ".passThrough", pull_locator + ".translate")

        #  parenting - to keep things organized in channel box
        maya.cmds.parent(tracer_txfrm, push_locator)
        maya.cmds.parent(pull_locator, push_locator)

        # ... to avoid the double transforms from parenting....
        maya.cmds.setAttr(tracer_txfrm + ".inheritsTransform", 0)
        maya.cmds.setAttr(pull_locator + ".inheritsTransform", 0)

        # Finally, parent-constrain the 'tracer rig' to the target
        maya.cmds.parentConstraint(target, push_locator)



def cmd_creator():
    """ Create an instance of the command. """
    return OpenMayaMPx.asMPxPtr(ScriptedCommand())


def initializePlugin(mobject):
    """ Initialize the plug-in when Maya loads it. """
    mplugin = OpenMayaMPx.MFnPlugin(mobject)
    try:
        mplugin.registerCommand(PLUGIN_CMD_NAME, cmd_creator)
    except:
        sys.stderr.write("Failed to register command: {}\n".format(PLUGIN_CMD_NAME))
        raise


def uninitializePlugin(mobject):
    """ Uninitialize the plug-in when Maya unloads it. """
    mplugin = OpenMayaMPx.MFnPlugin(mobject)
    try:
        mplugin.deregisterCommand(PLUGIN_CMD_NAME)
    except:
        sys.stderr.write("Failed to unregister command: {}\n".format(PLUGIN_CMD_NAME))
